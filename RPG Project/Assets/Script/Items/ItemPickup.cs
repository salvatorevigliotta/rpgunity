﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : Interactable
{
    public Item item;

    public override void Interact()
    {
        base.Interact();

        PickUp();

    }

    private void PickUp()
    {
        Debug.Log("Piking up " + item.name);

        bool wasPickedUp = Inventory.instance.AddItem(item);
        if(wasPickedUp)
            Destroy(gameObject);
    }
}
