﻿using UnityEngine.EventSystems;
using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    public Interactable Focus;
    public LayerMask movementMask;


    Camera cam;
    PlayerMotor motor;

	// Use this for initialization
	void Start ()
    {
        cam = Camera.main;
        motor = GetComponent<PlayerMotor>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

		if(Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, 100, movementMask))
            {
                motor.MoveToPoint(hit.point);

                RemoveFocus();
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Interactable interactble = hit.collider.GetComponent<Interactable>();
                if(interactble != null)
                {
                    SetFocus(interactble);
                }
            }

        }
    }

    private void RemoveFocus()
    {
        if(Focus != null)
            Focus.OnDefocused();

        Focus = null;
        motor.StopFollowingTarget();
    }

    private void SetFocus(Interactable newFocus)
    {
        if(newFocus != Focus)
        {
            if(Focus != null)
                Focus.OnDefocused();

            Focus = newFocus;
            motor.FollowTarget(newFocus);
        }
        
        newFocus.OnFocused(transform);
        
    }
}
