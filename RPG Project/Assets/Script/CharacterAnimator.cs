﻿using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour
{

    private const float SmoothTime = .1f;

    private Animator animator;
    private NavMeshAgent agent;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponentInChildren<Animator>();
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("SpeedPercent", speedPercent, SmoothTime, Time.deltaTime);
	}
}
