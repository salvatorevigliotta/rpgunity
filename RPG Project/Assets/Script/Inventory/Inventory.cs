﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    
#region Singleton

    public static Inventory instance;

    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        instance = this;
    }

    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public List<Item> Items = new List<Item>();
    public int InventorySpace = 20;

    public bool AddItem(Item item)
    {
        if(!item.isDefaultItem)
        {
            if(Items.Count >= InventorySpace)
            {
                Debug.Log("Not enough room. ");
                return false;
            }
            Items.Add(item);

            if(onItemChangedCallback != null)
                onItemChangedCallback.Invoke();

        }

        return true;

    }

    public void RemoveItem(Item item)
    {
        Items.Remove(item);

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }
}
